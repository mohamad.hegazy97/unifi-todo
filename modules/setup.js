const User = require("../db/modules/user");
const Task = require("../db/modules/task");

const randomString = (length) => {
	let alphabet = [...Array(26)].map((_, i) => String.fromCharCode(i + 97)).join("");
	let characters = []
		.concat(
			alphabet.split(""),
			alphabet.toUpperCase().split(""),
			[...Array(26)].map((_, i) => i + 1)
		)
		.join("");

	return [...Array(length)].map(() => characters.at(Math.floor(Math.random() * characters.length))).join("");
};

module.exports = async () => {
	const users = await User.countUsers();

	// Create demo user
	if (users === 0) {
		if (process.env.DEMO === "true") {
			const [user, error] = await User.createUser({
				password: "demo",
				email: "demo@example.com",
			});
			if (user != null) {
				for (var i = 1 ; i < 5 ; i++) {
					const [task, err] = await Task.createTask({
						userId: user.Id,
						taskBody: randomString(10),
						doDate: new Date()
					});
				}
			}
		}
    }

	return { setupCompleted: true };
};