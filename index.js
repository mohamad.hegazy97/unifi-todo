require("dotenv").config(".env");
const express = require("express");
const mongoSanitize = require("express-mongo-sanitize");
const bodyParser = require("body-parser");

const mongoose = require("mongoose");
const setup = require("./modules/setup");

let undefinedEnv = ["DB_USER", "DB_PASSWORD", "DB_HOST", "JWT_KEY", ].filter(
	(envFile) => !process.env.hasOwnProperty(envFile)
);

if (undefinedEnv.length > 0) {
	throw new Error(`Required env variables were not provided: ${undefinedEnv.join(",")}`);
}

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(mongoSanitize({ allowDots: true }));

app.use((req, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", process.env.CORS || "*");
	res.setHeader("Access-Control-Allow-Headers", "*");
	res.setHeader("Access-Control-Allow-Methods", "*");
	res.setHeader("Access-Control-Allow-Credentials", "*");
	next();
});

app.use("/api", require("./api"));

const mongoDB = `mongodb://${process.env.DB_HOST}:${
	process.env.DB_PORT
}/unifi`;

if (process.env.DB_USER) {
	mongoDB = `mongodb://${process.env.DB_USER}:${encodeURIComponent(process.env.DB_PASSWORD)}@${process.env.DB_HOST}:${
		process.env.DB_PORT
	}/unifi`;
}
console.log(mongoDB);

mongoose.set("strictQuery", false);

mongoose
	.connect(mongoDB, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		console.log("Connected to database");
		const listenPort = process.env.EXPRESS_PORT || 3000;
		setup().then(() => {
			app.listen(listenPort, async () => {
				console.log(`Listening on port ${listenPort}`);
			});
		});
	})
	.catch((error) => {
		console.log("Couldn't connect to database!");
		console.log(error);
	});