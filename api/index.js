const express = require("express");
const router = express.Router();

router.use("/auth", require("./auth.js"));

router.use("/task", require("./task.js"));

router.get("/", (req, res) => {
	res.send("OK");
});

module.exports = router;