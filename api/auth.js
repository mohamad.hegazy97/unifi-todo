const express = require("express");
const router = express.Router();
const requireFields = require("./middleware/requireFields");
const countUser = require("../db/modules/user/count");
const {
	login,
	register,
} = require("../db/modules/user");

router.post("/login", requireFields(["email", "password"]), async (req, res) => {
	try {
		const { email, password } = req.body;

		const [data, error] = await login({
			email,
			password,
		});

		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});

		res.setHeader("Set-Cookie", data.serialized);
		return res.status(200).json({
			success: true,
			message: "Successfully logged in!",
			data: {
				token: data.token
			}
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when logging in",
		});
	}
});

router.post("/register", requireFields(["email", "password"]), async (req, res) => {
	const userCount = await countUser();
	if ((userCount !== 0 && process.env.ENABLE_REGISTRATION !== "true") || process.env.DEMO == "true")
		return res.status(412).json({
			success: false,
			message: "Registration is not enabled",
		});
	try {
		const { email, password } = req.body;

		const [data, error] = await register({
			email,
			password,
		});

		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
				details: error.details,
			});

		return res.status(200).json({
			success: true,
			result: data,
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when registering",
		});
	}
});

module.exports = router;