const current = require("../../db/modules/user/current");

module.exports = (softFail) => {
	return async (req, res, next) => {
		let user;
		let error;
		if (req.headers.authorization && !req.headers.authorization.startsWith("Bearer")) {
            return res.status(403).json({
                success: false,
                message: "You cannot use this endpoint with your API secret",
            });
			
		} else {
			let token = null;
			if (req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
				token = req.headers.authorization.split(" ")[1];
			}
			[user, error] = await current(req, token);
		}

		if (error && !softFail)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});

		if (!error) req.user = user;

		next();
	};
};