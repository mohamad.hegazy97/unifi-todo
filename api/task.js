const express = require("express");
const router = express.Router();
const requireFields = require("./middleware/requireFields");
const requireLogin = require("./middleware/requireLogin");
const task = require("../db/modules/task")

router.post("", requireLogin(false), requireFields(["body"]), async (req, res) => {
	try {
		const { body, doDate } = req.body;

		const [data, error] = await task.createTask({
			userId: req.user.id,
            taskBody: body,
            doDate: doDate
            
		});

		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});

		return res.status(200).json({
			success: true,
			message: "task added Successfully!",
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when adding task",
		});
	}
});

router.get("/all", requireLogin(false), async (req, res) => {
	try {

		const [data, error] = await task.get.getUserTasks({
			userId: req.user.id            
		});

		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});

		return res.status(200).json({
			success: true,
            data: data,
			message: "task added Successfully!",
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when fetching tasks",
		});
	}
});

router.get("", requireLogin(false), async (req, res) => {
	try {
		let taskId = req.query.id;
		if (taskId == null) {
			return res.status(404).json({
				success: false,
				message: "Invalid id",
			});
		}
		const [data, error] = await task.get.getTaskById({
			userId: req.user.id,
			id: taskId
		});

		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});

		return res.status(200).json({
			success: true,
            data: data,
			message: "task added Successfully!",
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when fetching tasks",
		});
	}
});

router.delete("", requireLogin(false), async (req, res) => {
	try {
		let taskId = req.query.id;
		if (taskId == null) {
			return res.status(404).json({
				success: false,
				message: "Invalid id",
			});
		}
		let [data, error] = await task.get.getTaskById({
			userId: req.user.id,
			id: taskId
		});

		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});
		[data, error] = await task.delete({id:taskId, userId: req.user.id})
		return res.status(200).json({
			success: true,
            data: data,
			message: "task deleted Successfully!",
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when fetching tasks",
		});
	}
});


router.put("", requireLogin(false), async (req, res) => {
	try {
		
		const { body, doDate, id, state } = req.body;
		
		let [data, error] = await task.get.getTaskById({
			userId: req.user.id,
			id: id,
			withId:true
		});
		console.log(data);
		if (error)
			return res.status(error.code).json({
				success: false,
				message: error.message,
			});
		[data, error] = await task.update({task: data, body: body, doDate: doDate, state: state})
		return res.status(200).json({
			success: true,
            data: data,
			message: "task deleted Successfully!",
		});
	} catch (e) {
		console.log(e);
		return res.status(500).json({
			success: false,
			message: "Internal Server Error when fetching tasks",
		});
	}
});

module.exports = router;