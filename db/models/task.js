const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  userId: {
    type: String,
    required: true
  },
  task: {
    type: String,
    required: true
  },
  creationDate: {
    type: Date,
    default: Date.now
  },
  doDate: {
    type: Date,
    required: false
  },
  state: {
    type: String,
    enum: ['pending', 'completed'],
    default: 'pending'
  }
});

const Task = mongoose.model("Task", taskSchema);

module.exports = Task;
