module.exports = {
	createUser: require("./create"),
	login: require("./login"),
	register: require("./register"),
	countUsers: require("./count")
};