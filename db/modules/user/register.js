const User = require("../../models/user");
const valid = require("../valid");
const createUser = require("../user/create");

module.exports = async ({ email, password }) => {
	const invalid = {
		email: false,
		password: false,
	};

	invalid.email = !valid.email(email);
	invalid.password = !valid.password(password);

	if (invalid.password || invalid.email) {
		return [
			null,
			{
				code: 400,
				message: `Invalid field(s)`,
				details: { invalid },
			},
		];
	}

	const exists = {
		email: false,
	};

	const emailRegex = new RegExp(email.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "gi");
	exists.email = (await User.findOne({ email: emailRegex })) !== null;

	if (exists.email) {
		return [
			null,
			{
				code: 400,
				message: `Field(s) are already used`,
				details: { exists },
			},
		];
	}

	const [user, error] = await createUser({
		email,
		password,
	});

	if (user) return ["User created", null];
	if (!error)
		return [
			null,
			{
				code: 500,
				message: "Internal Server Error when registering User",
			},
		];
};