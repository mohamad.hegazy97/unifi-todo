const jwt = require("jsonwebtoken");
const User = require("./../../models/user");
const cookie = require("cookie");
require("dotenv").config();

module.exports = async (req, token) => {
	
	if (!token) {
		const cookies = cookie.parse(req.headers.cookie || '');
		if (!cookies.token) return [null, { code: 400, message: "No cookie token provided" }];
		token = cookies.token;
	}

	try {
		const decodedjwt = jwt.verify(token, process.env.JWT_KEY);
		const currentUser = await User.findOne({ id: decodedjwt.id });
		if (!currentUser) return [null, { code: 401, message: "Invalid authorization token" }];

		return [currentUser, null];
	} catch (err) {
		return [null, { code: 401, message: "Invalid authorization token" }];
	}
};