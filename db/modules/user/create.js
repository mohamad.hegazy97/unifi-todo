const User = require("../../models/user");
const hashPassword = require("../password/hash");
const { v4: uuid4 } = require("uuid");

module.exports = async ({email, password }) => {
	try {
		const user = new User({
			id: uuid4(),
			password: hashPassword(password),
			email,
		});

		await user.save();

		return [user, null];
	} catch (e) {
		console.log(e);
		return [null, true];
	}
};