const User = require("../../models/user");

module.exports = async () => {
	return await User.count();
};