const User = require("../../models/user");
const checkPassword = require("../password/check");
const jwt = require("jsonwebtoken");
const cookie = require("cookie");
require("dotenv").config("../../../.env");

const getUserByEmail = async (email) => {
	return await User.findOne({
		email,
	});
};

module.exports = async ({ email, password }) => {
	let errors = [];

	if (Object.keys(errors).length !== 0)
		return [
			null,
			{
				message: Object.values(errors).join("\n"),
				code: 400,
			},
		];

	const user = await getUserByEmail(email);
	if (!user)
		return [
			null,
			{
				message: "Invalid email or password",
				code: 403,
			},
		];
	const passwordIsCorrect = checkPassword(password, user?.password);
	if (!passwordIsCorrect)
		return [
			null,
			{
				message: "Invalid email or password",
				code: 403,
			},
		];

	// expire jwt logins in an hour if demo
	const expiresIn = process.env.DEMO === "true" ? "1h" : "7d";

	const token = jwt.sign(
		{
			id: user.id,
		},
		process.env.JWT_KEY,
		{
			algorithm: "HS256",
			expiresIn,
		}
	);

	const serialized = cookie.serialize("token", token, {
		httpOnly: true,
		secure: process.env.USE_HTTPS === 'true',
		sameSite: "strict",
		maxAge: process.env.DEMO ? 3600 * 1000 : 86400 * 1000 * 7,
		path: "/"
	})

	return [
		{
			serialized,
			token,
			user,
		},
		null,
	];
};