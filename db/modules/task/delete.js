const Task = require("../../models/task");

module.exports = async ({ id, userId }) => {

	try {
		await Task.deleteMany({
			id: id,
            userId: userId
		});
		return [true, null];
	} catch (e) {
		console.log(e);
		return [
			null,
			{
				code: 500,
				message: "Internal Server Error when deleting Link",
			},
		];
	}
};