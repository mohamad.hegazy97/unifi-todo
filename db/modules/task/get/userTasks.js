const Task = require("../../../models/task");

module.exports = async ({ userId }) => {
	const tasks = await Task.find({ userId }, {_id:0});
	if (tasks) return [tasks, null];

	return [null, { code: 404, message: "Invalid userId" }];
};