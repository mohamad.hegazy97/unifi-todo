const Task = require("../../../models/task");

module.exports = async ({ id, userId ,withId=false}) => {
	let fetchIdQuery = {_id:0}
	if (withId) {
		fetchIdQuery = {}
	}
	const task = await Task.findOne({ id:id, userId: userId }, fetchIdQuery);
	if (task) return [task, null];

	return [null, { code: 404, message: "Invalid id" }];
};