module.exports = {
	getTaskById: require("./byId"),
	getUserTasks: require("./userTasks")
};