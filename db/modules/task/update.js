
module.exports = async ({ task, body, doDate, state }) => {
    if (doDate) {
        task.doDate = new Date(doDate);
    }
    if (body) {
        task.task = body;
    }
    if (state) {
        task.state = state == 0 ? "pending" : "completed"
    }

    await task.save();

	return [
		{
			message: "task successfully updated",
			task: null,
		},
		null,
	];
};