const Task = require("../../models/task");
const { v4: uuid4 } = require("uuid");

module.exports = async ({userId, taskBody, doDate }) => {
	try {
		const task = new Task({
			id: uuid4(),
            userId: userId,
            task: taskBody,
            doDate: doDate,
            state: "pending",
		});
    
        
		await task.save();

		return [task, null];
	} catch (e) {
		console.log(e);
		return [null, true];
	}
};