module.exports = {
	createTask: require("./create"),
	get: require("./get"),
	update: require("./update"),
	delete: require("./delete")
};